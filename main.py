from todo_ng import TodoCLI

if __name__ == "__main__":
    cli = TodoCLI()
    while True:
        match input("Choose action (add/update/delete/list/quit): "):
            case 'add' | 'a':
                cli.add_todo()
            case 'update' | 'u':
                cli.update_todo()
            case 'delete' | 'd':
                cli.delete_todo()
            case 'list' | 'l':
                cli.list_todos()
            case 'quit'| 'q':
                break
