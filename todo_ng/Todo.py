
class Todo:
    def __init__(self, topic, details, priority, due_date=None, status='Pending'):
        self.topic = topic
        self.details = details
        self.priority = priority
        self.due_date = due_date
        self.status = status
