from todo_ng.Database import Database
from todo_ng.Todo import Todo


class TodoCLI:
    def __init__(self):
        self.db = Database()

    def add_todo(self):
        topic = input("Topic: ")
        details = input("Details: ")
        priority = input("Priority: ")
        due_date = input("Due Date (optional): ")
        todo = Todo(topic, details, priority, due_date)
        self.db.insert(todo)

    def update_todo(self):
        topic = input("Topic to update: ")
        new_values = {}
        new_values['details'] = input("New Details: ")
        new_values['priority'] = input("New Priority: ")
        new_values['status'] = input("New Status: ")
        self.db.update({'topic': topic}, new_values)

    def delete_todo(self):
        topic = input("Topic to delete: ")
        self.db.delete({'topic': topic})

    def list_todos(self):
        status_input = input("Filter by status (default is 'Pending'): ")
        query = {'status': status_input if status_input else 'Pending'}
        todos = self.db.find(query)
        for todo in todos:
            print(f"{todo['topic']} - {todo['details']} - {todo['priority']} - {todo['status']}")
