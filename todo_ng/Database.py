from pymongo import MongoClient

class Database:
    def __init__(self):
        self.client = MongoClient('mongodb://localhost:27017/')
        self.db = self.client['todo_db']
        self.collection = self.db['todos']

    def insert(self, todo):
        self.collection.insert_one(todo.__dict__)

    def update(self, query, new_values):
        self.collection.update_one(query, {'$set': new_values})

    def delete(self, query):
        self.collection.delete_one(query)

    def find(self, query):
        return self.collection.find(query)
